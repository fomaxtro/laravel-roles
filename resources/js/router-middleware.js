const matcher =  async (to) => {
  if (to.matched.some(record => record.meta.redirect)) {
    let response = await (async () => {
      try {
        return await axios.get(`${window.location.origin}/api/auth/user`)
      } catch (e) {
        return e.response;
      }
    })()

    if (response.status === 200 && to.path === '/login') {
      return {
        path: '/'
      }
    }

    return {}
  }

  if (to.matched.some(record => record.meta.auth)) {
    let response = await (async () => {
      try {
        return await axios.get(`${window.location.origin}/api/auth/user`)
      } catch (e) {
        return e.response;
      }
    })()

    if (response.status === 401) {
      return {
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      }
    }

    let credentials = response.data.preference.role.credentials
      .map(x => x.name)

    let needed = to.matched
      .flatMap(record => record.meta.credentials)
      .filter(x => x != null)

    if (needed.length > 0) {
      if (!credentials.includes('*')) {
        if (!needed.every(x => credentials.includes(x))) {
          return {
            path: '/forbidden'
          }
        }
      }
    }

    return credentials
  }

  return []
}

const filterRoutes = (credentials, routes) => {
  if (credentials.includes('*')) {
    return routes
  }

  return routes.filter(x => {
    if (x.hasOwnProperty('children')) {
      x.children = x.children.filter(z => {
        if (z.hasOwnProperty('credential')) {
          return credentials.includes(z.credential)
        }

        return false
      })

      return x.children.length > 0
    } else if (x.hasOwnProperty('credential')) {
      return credentials.includes(x.credential)
    } else {
      return false
    }
  })
}

export {matcher, filterRoutes}
