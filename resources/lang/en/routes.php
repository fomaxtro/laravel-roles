<?php

/*
|--------------------------------------------------------------------------
| Route Name Translation
|--------------------------------------------------------------------------
|
| This file contains the translations for each declared named routes
|
*/

return [
    '' => [
        // Translation of route
        'translation' => '',
        // Group translation for prefixed routes ex: name('group.route') -> group
        'children' => [
        ]
    ]
];