<?php

namespace Fomaxtro\Roles\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user) {
            return response()->json([
                'message' => 'Unauthenticated'
            ], 403);
        }

        if (!$user->preference) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);
        }

        if (!$user->preference->role) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);
        }

        $match = $user->preference
            ->role
            ->credentials()
            ->where(function ($q) {
                $q->where('name', Route::currentRouteName())
                    ->orWhere('name', '*');
            })
            ->exists();

        if (!$match) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);
        }

        return $next($request);
    }
}
