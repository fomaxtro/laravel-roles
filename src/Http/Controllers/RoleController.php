<?php

namespace Fomaxtro\Roles\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Fomaxtro\Roles\Credential;
use Fomaxtro\Roles\Helpers\Helpers;
use Fomaxtro\Roles\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            Role::query()
                ->withCount('credentials')
                ->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'credentials' => 'required|array'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            return DB::transaction(function () use ($request) {
                $routes = collect(Route::getRoutes())
                    ->filter(function ($value) { return !is_null($value->getName()); })
                    ->count();

                if ($routes == count($request->credentials)) {
                    Role::query()
                        ->create($request->all())
                        ->credentials()
                        ->sync(
                            Credential::query()
                                ->where('name',  '*')
                                ->pluck('id')
                        );

                    return response()->json([], 204);
                }

                $indexes = array_map(function ($value) {
                    return Credential::query()->firstOrCreate([
                        'name' => $value['route'],
                        'path' => $value['path']
                    ])->id;
                }, $request->credentials);

                Role::query()
                    ->create($request->all())
                    ->credentials()
                    ->sync($indexes);

                return response()->json([], 204);
            });
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Error while processing request'
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role, Request $request)
    {
        $credentials = $role->credentials->map(function ($value) {
            $name = explode('.', $value->name);

            if (count($name) == 2) {
                $name = Helpers::translate("routes.{$name[0]}.children.{$name[1]}.translation",
                    $name[1]);
            } else {
                $name = Helpers::translate("routes.{$name[0]}.translation", $name[0]);
            }

            return (object) [
                'name' => $name,
                'route' => $value->name,
                'path' => $value->path
            ];
        });

        if ($request->get('group', false)) {
            $credentials = $role->credentials->map(function ($value) {
                return (object) [
                    'name' => $value->name,
                    'path' => $value->path
                ];
            })->sortByDesc(function ($value) {
                return count(explode('.', $value->name));
            })->values();

            $credentials = Helpers::nestedGroup(
                0,
                count(explode('.', $credentials->get(0)->name)),
                $credentials
            );

            return response()->json([
                'name' => $role->name,
                'description' => $role->description,
                'credentials' => Helpers::rArray($credentials->toArray())
            ]);
        }

        return response()->json(array_merge($role->toArray(), [
            'credentials' => $credentials
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'credentials' => 'required|array'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            return DB::transaction(function () use ($request, $role) {
                $role->fill($request->all());
                $role->save();

                $routes = collect(Route::getRoutes())
                    ->filter(function ($value) { return !is_null($value->getName()); })
                    ->count();

                if ($routes == count($request->credentials)) {
                    $role->credentials()
                        ->sync(
                            Credential::query()
                                ->where('name',  '*')
                                ->pluck('id')
                        );

                    return response()->json([], 204);
                }

                $indexes =  array_map(function ($value) {
                    return Credential::query()->firstOrCreate([
                        'name' => $value['route'],
                        'path' => $value['path']
                    ])->id;
                }, $request->credentials);

                $role->credentials()->sync($indexes);

                return response()->json([], 204);
            });
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Error while processing request'
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return response()->json([], 204);
    }
}
