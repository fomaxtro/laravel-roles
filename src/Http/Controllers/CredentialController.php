<?php

namespace Fomaxtro\Roles\Http\Controllers;

use App\Http\Controllers\Controller;
use Fomaxtro\Roles\Helpers\Helpers;
use Illuminate\Support\Facades\Route;

class CredentialController extends Controller
{
    public function credentials()
    {
        $credentials = collect(Route::getRoutes())
            ->map(function ($value) {
                return (object) [
                    'name' => $value->getName(),
                    'path' => implode(':', $value->methods) . ':/' . $value->uri
                ];
            })
            ->filter(function ($value) { return !is_null($value->name); })
            ->sortByDesc(function ($value) {
                return count(explode('.', $value->name));
            })
            ->values();

        $credentials = Helpers::nestedGroup(
            0,
            count(explode('.', $credentials->get(0)->name)),
            $credentials
        );

        return [
            [
                'label' => Helpers::translate('routes.admin.translation', '*'),
                'route' => '*',
                'children' => Helpers::rArray($credentials->toArray())
            ]
        ];
    }
}
