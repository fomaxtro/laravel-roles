<?php

namespace Fomaxtro\Roles\Console\Commands;

use Fomaxtro\Roles\Credential;
use Illuminate\Console\Command;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install laravel-roles database components';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Credential::query()
                ->create([
                    'name' => '*'
                ]);

            $this->info('Installation successfully');
        } catch (\Exception $ex) {
            $this->error($ex->getMessage());
        }
    }
}
