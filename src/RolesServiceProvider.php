<?php

namespace Fomaxtro\Roles;

use Fomaxtro\Roles\Console\Commands\InstallCommand;
use Illuminate\Support\ServiceProvider;

class RolesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();

            $this->publishes([
                __DIR__.'/../database/migrations' => database_path('migrations')
            ], 'roles-migrations');

            $this->loadTranslationsFrom(__DIR__.'/../resources/lang/en', 'roles');

            $this->publishes([
                __DIR__.'/../resources/lang/en' => resource_path('lang/en')
            ], 'roles-translations');

            $this->publishes([
                __DIR__.'/../resources/js/components' => resource_path('js/components')
            ], 'roles-components');

            $this->publishes([
                __DIR__.'/../resources/js/plugins' => resource_path('js/plugins')
            ], 'roles-directives');

            $this->publishes([
                __DIR__.'/../resources/js' => resource_path('js')
            ], 'roles-router');

            $this->commands(InstallCommand::class);
        }
    }

    protected function registerMigrations()
    {
        if (Roles::$runsMigrations) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
    }
}
