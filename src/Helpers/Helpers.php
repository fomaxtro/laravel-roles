<?php

namespace Fomaxtro\Roles\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;

class Helpers
{
    private function __construct()
    {
    }

    public static function mapWithKeys(array $haystack, callable $callback)
    {
        $result = [];

        foreach ($haystack as $key => $value) {
            array_push($result, $callback($value, $key));
        }

        return $result;
    }

    public static function translate($key, $default = '')
    {
        if (Lang::has($key)) {
            return Lang::get($key);
        } else if (empty($default)) {
            return $key;
        } else {
            return $default;
        }
    }

    public static function nestedGroup(int $index, int $max, Collection $collection)
    {
        if ($index === $max - 1) {
            return $collection->map(function ($value) {
                $sub_str = explode('.', $value->name);

                return (object) [
                    'name' => $sub_str[count($sub_str ) - 1],
                    'route' => $value->name,
                    'path' => $value->path
                ];
            });
        } else {
            return $collection->groupBy(function ($value) use ($index) {
                $result =  explode('.', $value->name, $index + 2);

                if ($index < count($result) - 1) {
                    return $result[$index];
                } else {
                    return $value->name;
                }
            })->map(function ($value) use ($index, $max) {
                return Helpers::nestedGroup($index + 1, $max, $value)->toArray();
            });
        }
    }

    public static function rArray(array $haystack, $translation = 'routes')
    {
        return Helpers::mapWithKeys($haystack, function ($value, $key) use ($translation) {
            if ($value instanceof \stdClass) {
                return [
                    'label' => Helpers::translate($translation . '.' . $value->name . '.translation', $value->name),
                    'route' => $value->route,
                    'path' => $value->path
                ];
            } else if (count($value) == 1) {
                if (array_key_exists($key, $value) || str_contains($key, '.')) {
                    while (!($value instanceof \stdClass)) {
                        $value = array_first($value);
                    }

                    return [
                        'label' => Helpers::translate($translation . '.' . $value->name . '.translation', $value->name),
                        'route' => $value->route,
                        'path' => $value->path
                    ];
                }
            }

            return [
                'label' => Helpers::translate("{$translation}.{$key}.translation", $key),
                'children' => Helpers::rArray($value,  "{$translation}.{$key}.children")
            ];
        });
    }
}