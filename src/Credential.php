<?php

namespace Fomaxtro\Roles;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    public $timestamps = false;

    public $fillable = [
        'name', 'path'
    ];
}
