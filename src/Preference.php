<?php

namespace Fomaxtro\Roles;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $fillable = [
        'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
