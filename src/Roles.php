<?php


namespace Fomaxtro\Roles;


use Illuminate\Support\Facades\Route;

class Roles
{
    public static $runsMigrations = true;

    public static function ignoreMigrations()
    {
        static::$runsMigrations = false;

        return new static;
    }

    public static function routes($middleware = [], $except = [])
    {
        Route::prefix('api')
            ->middleware(array_merge(['api'], $middleware))
            ->namespace('Fomaxtro\Roles\Http\Controllers')
            ->group(function () use ($except) {
                Route::get('credentials', 'CredentialController@credentials')
                    ->name('credentials');

                Route::resource('roles', 'RoleController')
                    ->except(array_merge(['create', 'edit'], $except));
            });
    }
}